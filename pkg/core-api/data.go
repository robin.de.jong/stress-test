package coreapi

type Environment uint8

const (
	Dev Environment = iota
	Staging
	Production
	Local
)

type Session struct {
	ExpireTime         int    `json:"expire_time"`
	ID                 string `json:"id"`
	AuthenticationType int    `json:"authentication_type"`
}
