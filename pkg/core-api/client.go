package coreapi

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"
)

type CoreApiClient struct {
	env Environment
}

func NewClient(env Environment) *CoreApiClient {
	client := CoreApiClient{}
	client.env = env
	return &client
}

func (c *CoreApiClient) Session(username string, password string) (string, error) {
	token := c.getToken(c.env)
	headers := c.createHeaders("Authorization", fmt.Sprintf("CCToke %s:%s:%s", token, username, password))
	url := c.getUrl(c.env, "cc", "v1", "session")

	bytes, err := c.post(url, headers)
	if err != nil {
		return "", err
	}

	var session Session
	json.Unmarshal(bytes, &session)

	return session.ID, nil
}

func (c *CoreApiClient) ChannelPosts(token string, school int) error {
	headers := c.createHeaders("Authorization", fmt.Sprintf("CCSess %s", token))
	url := c.getUrl(c.env, "cc", "v1", "channel_post")
	url = fmt.Sprint(url, "1;101?parent_post_id=-1&use_rich_text=true&school_id=", school)

	bytes, err := c.get(url, headers)
	if err != nil {
		return err
	}

	log.Println("Bytes:", string(bytes))
	return nil
}

func (c *CoreApiClient) getToken(env Environment) string {
	return "JxG16vzPG7PrsjB4kdDNjlWoCCgs6fW9"
}

func (c *CoreApiClient) getUrl(env Environment, parts ...string) string {
	base := c.getBaseUrl(env)
	return base + "/" + strings.Join(parts, "/") + "/"
}

func (c *CoreApiClient) getBaseUrl(env Environment) string {
	switch env {
	case Dev:
		return "https://dev-web-api.studentlifemobile.com:5009"
	case Staging:
		return "https://usstagingapi.studentlifemobile.com"
	case Production:
		return "https://api.studentlifemobile.com"
	default:
		return "http://localhost:5001"
	}
}

func (c *CoreApiClient) post(url string, headers map[string]string) ([]byte, error) {
	req, err := http.NewRequest("POST", url, nil)
	if err != nil {
		return nil, err
	}

	for key, val := range headers {
		req.Header.Add(key, val)
	}

	response, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	if response.StatusCode == http.StatusCreated {
		return io.ReadAll(response.Body)
	} else {
		return nil, errors.New(
			fmt.Sprint("Login was not succesfull, with error code: ", response.StatusCode),
		)
	}

}

func (c *CoreApiClient) get(url string, headers map[string]string) ([]byte, error) {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	for key, val := range headers {
		req.Header.Add(key, val)
	}

	response, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	if response.StatusCode == http.StatusCreated {
		return io.ReadAll(response.Body)
	} else {
		return nil, errors.New(
			fmt.Sprint("Login was not succesfull, with error code: ", response.StatusCode),
		)
	}
}

func (c *CoreApiClient) createHeaders(key string, value string) map[string]string {
	headers := make(map[string]string)
	headers[key] = value
	return headers
}
