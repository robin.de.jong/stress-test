package main

import (
	"log"
	coreapi "stress-test/pkg/core-api"
)

const Channel = 1
const School = 47727

func main() {
	log.Println("Hello World!")

	coreapi := coreapi.NewClient(coreapi.Staging)
	token, err := coreapi.Session("robin.de.jong@readyeducation.com", "9AfFZc29msKGPJFKtHZrXYtUyUYwizuDoJ9dWHRjFXbbfBvz")
	if err != nil {
		log.Fatalln("Error when getting the session:", err)
	}
	log.Println("Got token:", token)

	err = coreapi.ChannelPosts(token, School)
	if err != nil {
		log.Fatalln("Error fetching posts:", err)
	}
}
